import time
import board
import busio
global i2c 
i2c = busio.I2C(board.SCL, board.SDA)

import adafruit_ssd1306_old
global display 
display = adafruit_ssd1306_old.SSD1306_I2C(128, 32, i2c)

import adafruit_ina219
global sensor
sensor = adafruit_ina219.INA219(i2c)

import machine
global a_button
global b_button
global c_button
a_button = machine.Pin(0, machine.Pin.IN, machine.Pin.PULL_UP)
b_button = machine.Pin(16, machine.Pin.IN)
c_button = machine.Pin(2, machine.Pin.IN, machine.Pin.PULL_UP)

#Define helper functions for CircuitPython
def clear():
	print("\x1B\x5B2J", end="")
	print("\x1B\x5BH", end="")
		
def running_avg(trigger_level):
	i = 0
	cur = 0
	trig = 1
	prev_c = sensor.current
	peak_c = 0
	prev_v = sensor.bus_voltage
	min_v = 3.3
	avg = 0
	#trigger_level = 10
	run_time = time.monotonic()
	run_time_trig = 0
	time_avg = 0
	time_avg_count = 0
	cur_avg_total = 0
	cur_avg_total_count = 0
	while a_button.value() == 1:
		if (b_button.value() == 0) or ((sensor.current < trigger_level) and (trig == 0)):
			i = 0
			cur = 0
			trig = 1
			time_run = time.monotonic()-run_time
			time_avg += time_run
			time_avg_count += 1
			cur_avg_total += avg
			cur_avg_total_count += 1
			
			#print("I_p: ", end =""), print(peak_c, end =" I_avg: "), print(avg, end =" V_min: "), print(min_v, end =" S: "), print(time.monotonic()-run_time, end=""), print("          ")
			print('I_p: {:6.2f}mA  I_avg: {:6.2f}mA  I_avg_total: {:6.2f}mA  V_min: {:3.2f}V  Time: {:3.2f}S  Time_avg: {:3.2f}S     '.format(peak_c, avg, cur_avg_total/cur_avg_total_count, min_v, time_run, time_avg/time_avg_count))
			print('---------------')
			
			avg = 0
			peak_c = 0
			min_v = 3.3
			prev_c = 0
			run_time_trig = 0
			
			oled_clear()
			display.text('I_avg: {:6.2f}mA'.format(cur_avg_total/cur_avg_total_count), 0, 0, 1)
			display.text('T_avg: {:6.2f}s'.format(time_avg/time_avg_count), 0, 10, 1)
			display.text('Count: {:6}'.format(time_avg_count), 0, 20, 1)
			display.show()
		if(sensor.current > trigger_level):
			if(run_time_trig == 0):
				run_time = time.monotonic()
				run_time_trig = 1
			i += 1
			cur += sensor.current
			prev_c = sensor.current
			prev_v = sensor.bus_voltage
			if(prev_c > peak_c):
				peak_c = prev_c
			if(prev_v < min_v):
				min_v = prev_v				
			avg = cur/i
			trig = 0
			#print("I: ", end =""), print(prev_c, end =" I_p: "), print(peak_c, end =" I_avg: "), print(avg, end =" V: "), print(prev_v, end=" S: "), print(time.monotonic()-run_time, end ="\r")
			print('I: {:6.2f}mA  I_p: {:6.2f}mA  I_avg: {:6.2f}mA  Time: {:3.2f}S      '.format(prev_c, peak_c, avg, time.monotonic()-run_time), end="\r")
		time.sleep(0.0001)

def oled_clear():
	display.fill(0)
	display.show()
	
if __name__ == "__main__":
	running_avg(10)

def oled_fill():
	display.fill(1)
	display.show()